
package editor_proyecto;

import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.DefaultListModel;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


public class Ventana extends javax.swing.JFrame {
    private final String fuentes[];
    private DefaultListModel dlm;
    
    public Ventana() {
        dlm = new DefaultListModel();
        fuentes = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        initComponents();
        
        lista.setModel(dlm);
        cargarComponentes();
        jTextArea1.requestFocus();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.setTitle("Tusa Editor");
    }
    
    private void cargarComponentes(){
        for(int i = 10; i<=60; i++){
            cbTletra.addItem(i);
        }
        for(String fuente: fuentes){
            dlm.addElement(fuente);
        }
    }
       private void cerrar(){
        String botones[]={"Guardar","No Guardar"};        
        int eleccion =JOptionPane.showOptionDialog(this, "¿Quieres guardar los cambios?", "Seleccione una opción", 0, 0, null, botones, this);
        if(eleccion==JOptionPane.YES_OPTION){
            JFileChooser fc = new JFileChooser();
            int seleccion = fc.showSaveDialog(this);
            if(seleccion == JFileChooser.APPROVE_OPTION){
                File fichero = new File(fc.getSelectedFile()+".txt");
                try (FileWriter fw = new FileWriter(fichero)){
                    fw.write(this.jTextArea1.getText());
                    JOptionPane.showMessageDialog(this, "Archivo Guardado");
                    System.exit(0);
                }catch (IOException e){
                }
            }
        }else if(eleccion==JOptionPane.NO_OPTION){
            System.exit(0);
        }       
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPopupMenu1 = new javax.swing.JPopupMenu();
        MenuIcopiar = new javax.swing.JMenuItem();
        MenuIcortar = new javax.swing.JMenuItem();
        MenuIpegar = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        btnnuevo = new javax.swing.JButton();
        btnabrir = new javax.swing.JButton();
        btnguardar = new javax.swing.JButton();
        btnlimpiar = new javax.swing.JButton();
        btnnegrita = new javax.swing.JButton();
        btncolorl = new javax.swing.JButton();
        btncursiva = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        lista = new javax.swing.JList<>();
        cbTletra = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        MenuIcopiar.setText("Copiar");
        MenuIcopiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuIcopiarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MenuIcopiar);

        MenuIcortar.setText("Cortar");
        MenuIcortar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuIcortarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MenuIcortar);

        MenuIpegar.setText("Pegar");
        MenuIpegar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuIpegarActionPerformed(evt);
            }
        });
        jPopupMenu1.add(MenuIpegar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jTextArea1.setColumns(20);
        jTextArea1.setLineWrap(true);
        jTextArea1.setRows(5);
        jTextArea1.setWrapStyleWord(true);
        jTextArea1.setComponentPopupMenu(jPopupMenu1);
        jScrollPane1.setViewportView(jTextArea1);

        btnnuevo.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnnuevo.setText("Nuevo");
        btnnuevo.setBorder(null);
        btnnuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnuevoActionPerformed(evt);
            }
        });

        btnabrir.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnabrir.setText("Abrir");
        btnabrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnabrirActionPerformed(evt);
            }
        });

        btnguardar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardarActionPerformed(evt);
            }
        });

        btnlimpiar.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btnlimpiar.setText("Limpiar");
        btnlimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlimpiarActionPerformed(evt);
            }
        });

        btnnegrita.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        btnnegrita.setText("Negrita");
        btnnegrita.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnnegritaActionPerformed(evt);
            }
        });

        btncolorl.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        btncolorl.setText("Color de Letra");
        btncolorl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncolorlActionPerformed(evt);
            }
        });

        btncursiva.setFont(new java.awt.Font("Times New Roman", 2, 18)); // NOI18N
        btncursiva.setText("Cursiva");
        btncursiva.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncursivaActionPerformed(evt);
            }
        });

        lista.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                listaValueChanged(evt);
            }
        });
        jScrollPane2.setViewportView(lista);

        cbTletra.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbTletraItemStateChanged(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Times New Roman", 0, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("TUSA EDITOR");

        jLabel2.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Fuente");

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Tamaño de Letra");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(102, 102, 102)
                        .addComponent(jLabel2))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addContainerGap(140, Short.MAX_VALUE)
                            .addComponent(btnlimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGap(9, 9, 9)
                            .addComponent(jLabel3)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGap(19, 19, 19)
                            .addComponent(cbTletra, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(0, 15, Short.MAX_VALUE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(btnnuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(19, 19, 19)
                                    .addComponent(btnabrir, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addComponent(btnnegrita, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(btncursiva, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btncolorl, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 35, Short.MAX_VALUE)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 827, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnnuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnabrir))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnguardar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnnegrita, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btncursiva, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btncolorl, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbTletra, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnlimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 577, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(34, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnnuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnuevoActionPerformed
                  
        if(jTextArea1.getText().isEmpty()){//si el textArea esta vacia
            jTextArea1.setText("");
            jTextArea1.requestFocus();
            
        }else {
           int i= JOptionPane.showConfirmDialog(this,"¿Quieres guardar los cambios?");
           if(i == 0){
            JFileChooser fc = new JFileChooser();
            int seleccion = fc.showSaveDialog(this);
            if(seleccion == JFileChooser.APPROVE_OPTION){
                File fichero = new File(fc.getSelectedFile()+".txt");
                try (FileWriter fw = new FileWriter(fichero)){
                    fw.write(this.jTextArea1.getText());
                    JOptionPane.showMessageDialog(this, "Archivo Guardado");
                    jTextArea1.setText("");
                    jTextArea1.requestFocus();
                }catch (IOException e){  
                }
            }
              ///si selecciona si mandar a guardar 
           }else if(i==1){
               jTextArea1.setText("");
           }
        }
    }//GEN-LAST:event_btnnuevoActionPerformed

    private void btnguardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardarActionPerformed
          JFileChooser fc = new JFileChooser();
            int seleccion = fc.showSaveDialog(this);
            if(seleccion == JFileChooser.APPROVE_OPTION){
                File fichero = new File(fc.getSelectedFile()+".txt");
                try (FileWriter fw = new FileWriter(fichero)){
                    fw.write(this.jTextArea1.getText());
                    JOptionPane.showMessageDialog(this, "Archivo Guardado");
                    jTextArea1.setText("");
                }catch (IOException e){
                }
            }
    }//GEN-LAST:event_btnguardarActionPerformed

    private void btnabrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnabrirActionPerformed
        JFileChooser fc = new JFileChooser();
        int seleccion = fc.showOpenDialog(this);
        if(seleccion==JFileChooser.APPROVE_OPTION){
          File fichero = fc.getSelectedFile();
          try(FileReader fr = new FileReader(fichero)){
              String cadena = "";
              int valor = fr.read();
              while(valor !=-1){
                  cadena = cadena + (char) valor;
                  valor = fr.read();
              }
              this.jTextArea1.setText(cadena);
          }catch (IOException e){
          }
        }
    }//GEN-LAST:event_btnabrirActionPerformed

    private void listaValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_listaValueChanged
        Font f = jTextArea1.getFont();
        jTextArea1.setFont(new Font(String.valueOf(dlm.getElementAt(lista.getSelectedIndex())), Font.PLAIN, f.getSize()));
    }//GEN-LAST:event_listaValueChanged

    private void btnlimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlimpiarActionPerformed
        jTextArea1.setText("");
        jTextArea1.requestFocus();
    }//GEN-LAST:event_btnlimpiarActionPerformed
Color color;
    private void btncolorlActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncolorlActionPerformed
       color = JColorChooser.showDialog(null, "Selecciona un color", Color.BLACK);
        jTextArea1.setForeground(color);
    }//GEN-LAST:event_btncolorlActionPerformed

    private void cbTletraItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cbTletraItemStateChanged
        Font f = jTextArea1.getFont();
        jTextArea1.setFont(new Font(f.getName(), Font.PLAIN, Integer.parseInt(String.valueOf(cbTletra.getSelectedItem()))));
    }//GEN-LAST:event_cbTletraItemStateChanged

    private void btncursivaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncursivaActionPerformed
        Font f=new Font("Monospaced",Font.ITALIC,14); 
        jTextArea1.setFont(f);
    }//GEN-LAST:event_btncursivaActionPerformed

    private void btnnegritaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnnegritaActionPerformed
        Font f=new Font("Monospaced",Font.BOLD,14); 
     jTextArea1.setFont(f);
    }//GEN-LAST:event_btnnegritaActionPerformed

    private void MenuIcopiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuIcopiarActionPerformed
        jTextArea1.copy();
    }//GEN-LAST:event_MenuIcopiarActionPerformed

    private void MenuIcortarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuIcortarActionPerformed
        jTextArea1.cut();
    }//GEN-LAST:event_MenuIcortarActionPerformed

    private void MenuIpegarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuIpegarActionPerformed
        jTextArea1.paste();
    }//GEN-LAST:event_MenuIpegarActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if(jTextArea1.getText().isEmpty()){
            System.exit(0);
        }else{
            cerrar();
        }
    }//GEN-LAST:event_formWindowClosing

   
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new Ventana().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem MenuIcopiar;
    private javax.swing.JMenuItem MenuIcortar;
    private javax.swing.JMenuItem MenuIpegar;
    private javax.swing.JButton btnabrir;
    private javax.swing.JButton btncolorl;
    private javax.swing.JButton btncursiva;
    private javax.swing.JButton btnguardar;
    private javax.swing.JButton btnlimpiar;
    private javax.swing.JButton btnnegrita;
    private javax.swing.JButton btnnuevo;
    private javax.swing.JComboBox cbTletra;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPopupMenu jPopupMenu1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JList<String> lista;
    // End of variables declaration//GEN-END:variables
}
